################################################################
#
#    Generative Modeling Project 5 Kyokuhi for Blender 2.70
#    Coded by Shigeto 'Shige' Maeda  2014-03-26
#    s.maeda@oud-japan.co.jp
#
################################################################

from bpy import *
import bpy
import math, mathutils, random

matNum = 0
mData = []
lengthList = []

class GMP_Panel(bpy.types.Panel):
	bl_label = "*** Generator"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"
	
	def draw(self, context):
		layout = self.layout		
		scn = bpy.context.scene		
		row = layout.row()
#		'''
		row = row.column()
		row.label(text="Parameters")
		
		row = row.column()
		row.prop( scn, "legNum" )
		
		row = row.column()
		row.prop( scn, "LEG" )
				
		row = row.column()
		row.prop( scn, "underType_01" )
		
		row = row.column()
		row.prop( scn, "underType_02" )

		row = row.column()
		row.prop( scn, "upperType" )
		
		row = row.column()
		row.prop( scn, "sideType" )
		
		row = row.column()
		row.prop( scn, "topType" )
		
#		'''
		row = row.column()
		row.label(text=" ")
		
		row.operator( "bpt.generate_op" )

class GMP_Operator(bpy.types.Operator):
	bl_idname = "bpt.generate_op"
	bl_label = "Generate"
 
	def execute(self, context):
		bpy.ops.object.select_all(action='SELECT')
		bpy.ops.object.delete(use_global=False)

#		seedNum = bpy.context.scene.LEG
		legNum = bpy.context.scene.legNum
		legType = bpy.context.scene.LEG
		underType01 = bpy.context.scene.underType_01
		underType02 = bpy.context.scene.underType_02
		upperType = bpy.context.scene.upperType
		sideType = bpy.context.scene.sideType
		topType = bpy.context.scene.topType
		
		del lengthList[:]
		generate(legNum, legType, underType01, underType02, upperType, sideType, topType)
			
		#bpy.ops.object.hide_view_clear()
		
		return {'FINISHED'}

def register():
	bpy.utils.register_class( GMP_Operator )
	bpy.utils.register_class( GMP_Panel )
 
def unregister():
	bpy.utils.register_class( GMP_Operator )
	bpy.utils.register_class( GMP_Panel )

#-------------------------------------------------------- Main Code from here
# extrudeOneStep ---------------------------------------->
def extrudeOneStep(oneStep):
	dZ = oneStep[0] #Kyori
	sX = oneStep[1] #Scale-X
	sY = oneStep[2] #Scale-Y
	rA = oneStep[3] #RotatonAngle
	aX = oneStep[4] #Axis-X
	aY = oneStep[5] #Axis-Y
	aZ = oneStep[6] #Axis-Z
	
	selMode2FA()
#	bpy.ops.object.mode_set(mode='EDIT')
	ob = bpy.context.object
	# Scale control by face(area) size-->
	context = bpy.context
	if (context.active_object != None):
		object = bpy.context.active_object
	
	# Get the mesh
	me = ob.data
	# Loop through the faces to find the center
	rotDir = 1	
				
	for p in me.polygons:
		if p.select == True:
			px = p.normal[0]
			py = p.normal[1]
			pz = p.normal[2]
			p_center = [0.0, 0.0, 0.0]
			for v in p.vertices:
				p_center[0] += me.vertices[v].co[0]        
#				p_center[1] += me.vertices[v].co[1]
#				p_center[2] += me.vertices[v].co[2]
			p_center[0] /= len(p.vertices) # division by zero               
#			p_center[1] /= len(p.vertices) # division by zero               
#			p_center[2] /= len(p.vertices) # division by zero       
			if p_center[0] > 0.0:
				rotDir = -1.0
			else:
				rotDir = -1.0
			
	selMode2Edit()
	
#	bpy.ops.transform.shrink_fatten(value=dZ * sX, mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1.0, snap=False, snap_target='CLOSEST', snap_point=(0.0, 0.0, 0.0), snap_align=False, snap_normal=(0.0, 0.0, 0.0), release_confirm=False)			
#	bpy.ops.transform.resize(value=(sX, sY, sX), constraint_axis=(False, False, False), constraint_orientation='NORMAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=0.0762767, snap=False, snap_target='CLOSEST', snap_point=(0, 0, 0), snap_align=False, snap_normal=(0, 0, 0), texture_space=False, release_confirm=False)
#	bpy.ops.transform.rotate(value=(rA*rotDir), axis=(aX, aY, aZ), constraint_axis=(False, False, False), constraint_orientation='NORMAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=0.0762767, snap=False, snap_target='CLOSEST', snap_point=(0, 0, 0), snap_align=False, snap_normal=(0, 0, 0), release_confirm=False)

	#bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":dZ * sX, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})
	bpy.ops.transform.shrink_fatten(value=dZ * sX, mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1.0, snap=False, snap_target='CLOSEST', snap_point=(0.0, 0.0, 0.0), snap_align=False, snap_normal=(0.0, 0.0, 0.0), release_confirm=False)
	bpy.ops.transform.resize(value=(sX, sY, sX), constraint_axis=(True, True, True), constraint_orientation='NORMAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=0.0762767, snap=False, snap_target='CLOSEST', snap_point=(0, 0, 0), snap_align=False, snap_normal=(0, 0, 0), texture_space=False, release_confirm=False)
#	bpy.context.space_data.transform_orientation = 'NORMAL'
#	bpy.context.space_data.transform_manipulators = {'TRANSLATE', 'ROTATE'}
	
	bpy.ops.transform.rotate(value=rA*rotDir, axis=(px, py, 0), constraint_axis=(True, True, True), constraint_orientation='NORMAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1, release_confirm=True)

	
	bpy.ops.object.vertex_group_remove_from(use_all_groups=True)
	
	bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
	
# extrudeOneStep(END) ----------------------------------------<

# multiExtrudeByMaterialNum() ---------------------------------->
def multiExtrudeByMaterialNum(mltData):
	vGroup = mltData[0] 
	mainLevel = mltData[1]
	stepNum = mltData[2] 
	stepDist = mltData[3] # +random.uniform(-0.1, 0.0)
	stepRot = mltData[4] # +random.uniform(-5, 5)
	startDeg = mltData[5] # +random.uniform(-5, 5)
	stepDeg = mltData[6] # +random.uniform(-5, 5)
	modLevel = mltData[7] # +random.uniform(-0.1, 0.1)
	modRate = mltData[8] # +random.uniform(-0.1, 0.1)
	axis = mltData[9]
	jointNum = mltData[10]
	fPers = mltData[11]
	areaLimit = mltData[12]
	
	ob = bpy.context.object
	
	# Make sure nothing is selected
#	selMode2Edit()
	# Select the vertex group
#	selPart(vGroup)
	
	# Go into object mode so that we are sure we have the current list of faces 
	selMode2Object()
	# Make a nice list to keep the vertex groups in
	groupList = []

	# Now loop through all the faces
	for i, f in enumerate(ob.data.polygons):
		
		# If this face is selected
#		if ob.data.polygons[i].select == True:
		if ob.data.polygons[i].select == True and ob.data.polygons[i].area > areaLimit:

			# Create a new vertex group!
			newGroup = ob.vertex_groups.new('mygroup')
			groupList.append(newGroup)

			# Get all the vertices in the current face and add them to the new group
			for v in f.vertices:
				newGroup.add([v], 1.0, 'REPLACE')
			
	# Now we loop through the groups and do what we want.
	for g in groupList:

		# Make sure nothing is selected
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_all(action='DESELECT')

		# Make the group in our list the active one
		ob.vertex_groups.active_index = g.index

		# Select all the verts in the active group
		bpy.ops.object.vertex_group_select()

		bpy.ops.object.vertex_group_remove(all=False)	

		# AND NOW WE CAN DO OUR STUFF... little test example follows
		t = startDeg
		oldValue = 1.0
		oldValue2 = 1.0
		# Shape
		j = 0
		for i in range(stepNum):
     
#			bpy.ops.mesh.extrude(type='REGION')
			bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
			
			# An added trick... remove the extruded face from all vertex groups after the first extrusion (i == 0)
			# This way it can't get extruded again
			# Because the edge of the first face can be part of multiple groups
			if not i:
#				bpy.ops.object.vertex_group_remove_from(all=True)
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)
			bpy.ops.object.mode_set(mode='OBJECT')
			bpy.ops.object.mode_set(mode='EDIT')
			
			newValue = (math.sin(math.radians(t)+math.sin(math.radians(t/1.13)))+2.0)*mainLevel  #*2
			newValue2 = (math.sin(math.radians(t*0.5)+math.sin(math.radians(t/0.97)))+2.0)*mainLevel
			newRatio = newValue/oldValue
			newRatio2 = newValue2/oldValue2
				
			# Get the mesh
			me = ob.data
			# Loop through the faces to find the center
			area = 1
			for f in me.polygons:
#				if f.select == True and vGroup != 'body' and vGroup != 'spike' and vGroup != 'pit':
				if f.select == True:
					area = math.sqrt(f.area) * 3
			
			dA = stepDist * area
			sA = newRatio
			sA2 = newRatio2
			rA = modLevel*math.sin(math.radians(t*modRate))*(-stepRot/10)
#			rA = math.sin(t*0.3) * 0.5
			axis_x = axis[0]
			axis_y = axis[1]
			axis_z = axis[2]
			#axis_z = 0.5
			#rZ = math.sin(math.radians(i*45 + bodyNum+45))*0.5 #<---------------------------------------
			rZ = 0
				
			morphData = [dA, sA, sA, rA, axis_x, axis_y, axis_z,rZ]
			extrudeOneStep(morphData)
			if jointNum != 0:
				j = j+1
				if j == jointNum:
					bpy.ops.mesh.extrude_region(mirror=False)
					morphData = [-0.01, 0.6, 0.6, 0, axis_x, axis_y, axis_z,0]
					extrudeOneStep(morphData) # <-------------0
					bpy.ops.mesh.extrude_region(mirror=False)
					morphData = [-0.01, 1.66, 1.66, 0, axis_x, axis_y, axis_z,0]
					extrudeOneStep(morphData) # <-------------0
					j = 0
			t = t + stepDeg
			oldValue = newValue
			oldValue2 = newValue2
			
		if vGroup != 0:
			bpy.context.object.active_material_index = vGroup
			bpy.ops.object.material_slot_assign()
			
	bpy.ops.object.mode_set(mode='EDIT')
			
# multiExtrudeByMaterialNum(END) -------------------------------<

# growStage ------------------------------------------------------>
def growStage(n, legType):
	
	selMode2Edit()
	bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 1.0), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
	bpy.ops.transform.resize(value=(1.5, 1.5, 1.5), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
	bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0.4), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
	bpy.ops.transform.resize(value=(0.7, 0.7, 0.7), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
	
	bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0.8), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
	bpy.ops.transform.resize(value=(0.5, 0.5, 0.5), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
	
	
	#set the materials to faces
	selMode2Object()
	deselectAllMesh()
	ob = bpy.context.object
	me = ob.data
	for p in me.polygons:
		if p.index < n:
			p.material_index = 1
		elif p.index < n * 2:
			p.material_index = 2
		elif p.index < n * 3:
			p.material_index = 3
		elif p.index < n * 4:
			p.material_index = 4
		else:
			p.material_index = 5
			
	for p in me.polygons:
#		if p.material_index == 3:
		if n * 3 > p.index >= n * 2:
			p.select = True
	selMode2Edit()
#	for i in range(5):
#		bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0.533533, -0.243683, -0.255627), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
#		bpy.context.space_data.transform_orientation = 'NORMAL'
#		bpy.context.space_data.transform_manipulators = {'TRANSLATE', 'ROTATE'}
#		bpy.ops.transform.rotate(value=0.414536, axis=(0.257412, 0.908608, -0.328893), constraint_axis=(False, True, False), constraint_orientation='NORMAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1, release_confirm=True)

#	Making Legs #####
	
	mData = [ [0, 0.6, 8, -0.2, -10.0, -290.0, -80.0, 0.1, 0.2, [-1, 0, 0], 0, 0, 0],      [0, 0.3, 8, -0.4, 30.0, -120.0, 60.0, 0.2, 0.5, [-1, 0, 0], 0, 0, 0],                      [0, 0.4, 8, -0.3, 30.0, -90.0, 60.0, 0.3, 0.6, [-1, 0, 0], 0, 0, 0],                        [0, 0.4, 8, -0.2, 30.0, -60.0, 60.0, 0.2, 0.7, [-1, 0, 0], 0, 0, 0],                        [0, 0.5, 8, -0.2, 10.0, -30.0, 50.0, 0.3, 0.2, [-1, 0, 0], 0, 0, 0],                         [0, 0.4, 10, -0.2, -20.0, -30.0, 40.0, 0.2, 0.9, [-1, 0, 0], 0, 0, 0],                          [0, 0.4, 8, -0.3, -10.0, 30.0, 50.0, 0.3, 0.3, [-1, 0, 0], 0, 0, 0],                         [0, 0.4, 8, -0.3, -20.0, -120.0, 40.0, 0.3, 0.3, [-1, 0, 0], 0, 0, 0],                     [0, 0.4, 8, -0.3, -30.0, -150.0, 30.0, 0.3, 0.3, [-1, 0, 0], 0, 0, 0],                     [0, 0.6, 8, -0.24, 3.0, -160.0, 20.0, 0.3, 1.0, [-1, 0, 0], 0, 0, 0],   ]
	
	morphData = mData[legType]
	multiExtrudeByMaterialNum(morphData)
	
##	aaa
# growStage(END) ---------------------------------------------------<
	
# selectColoredFace() ---------------------------------->
def selectColoredFace():
	
	ob = bpy.context.object
	# Make sure nothing is selected
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_all(action='DESELECT')
	selMode2FA()
	
	# Go into object mode so that we are sure we have the current list of faces 
	bpy.ops.object.mode_set(mode='OBJECT')
	
	for i, p in enumerate(ob.data.polygons):
		
		# If this face is NOT black
		if ob.data.polygons[i].material_index != 0:
			
			ob.data.polygons[i].select = True
				
# selectColoredFace(END) -------------------------------<

# firstExtrude() ---------------------------------->
def firstExtrude(mltData):
	mNum = mltData[0] 
	mainLevel = mltData[1] 
	stepNum = mltData[2] 
	stepDist = mltData[3] +random.uniform(-0.3, 0.3)
	stepRot = mltData[4] +random.uniform(-10, 10)
	startDeg = mltData[5] +random.uniform(-10, 10)
	stepDeg = mltData[6] +random.uniform(-10, 10)
	modLevel = mltData[7] +random.uniform(-0.3, 0.3)
	modRate = mltData[8]+random.uniform(-0.3, 0.3)
	axis = mltData[9]
	jointNum = mltData[10]
	fPers = mltData[11]
	mLevel = mltData[12]
	
	ob = bpy.context.object
	# Make sure nothing is selected
	#bpy.ops.object.mode_set(mode='EDIT')
	#bpy.ops.mesh.select_all(action='DESELECT')
	selMode2FA()
	
	# Go into object mode so that we are sure we have the current list of faces 
	bpy.ops.object.mode_set(mode='OBJECT')
	
	# Make a nice list to keep the vertex groups in
	groupList = []
	del lengthList[:]
	# Now loop through all the faces
	fNum = 0

	for i, p in enumerate(ob.data.polygons):
		
		# If this face is selected
		if ob.data.polygons[i].select == True:
			p.material_index = 0
			lengthList.append(random.randint(5, 10))
			# Create a new vertex group!
			vGroup = 'mygroup' + str(fNum)
			ob = bpy.context.object
			newGroup = ob.vertex_groups.new(vGroup)
			bpy.ops.object.vertex_group_set_active(group=vGroup)
			bpy.ops.object.vertex_group_remove(all=False)
			newGroup = ob.vertex_groups.new(vGroup)
			bpy.ops.object.vertex_group_set_active(group=vGroup)
			groupList.append(newGroup)
#			lengthList.append(random.randint(0, 6))
			# Get all the vertices in the current face and add them to the new group
			for v in p.vertices:
				newGroup.add([v], 1.0, 'REPLACE')
			fNum += 1
			
#	print(lengthList)
		
	tempNum = 0		
	for g in groupList:
	
		# Make sure nothing is selected
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_all(action='DESELECT')
		
		# Make the group in our list the active one
		ob.vertex_groups.active_index = g.index

		# Select all the verts in the active group
		bpy.ops.object.vertex_group_select()
		bpy.ops.object.vertex_group_remove(all=False)	

		# AND NOW WE CAN DO OUR STUFF... little test example follows
		t = startDeg
		oldValue = 1.0
		oldValue2 = 1.0
		# Shape
		j = 0
		if stepNum == 99:
			RandStepNum = random.randint(6, 20)
		else:
			RandStepNum = stepNum
		
		#dirFace = -1**(random.randint(0, 2))
		dirFace = random.uniform(0.3, 1.0)**random.randint(0, 2)
		axis = [random.uniform(-1.0,1.0),random.uniform(-1.0,1.0),random.uniform(-1.0,1.0)]
#		for i in range(RandStepNum):
     
		bpy.ops.mesh.extrude_region(mirror=False)
			
		# An added trick... remove the extruded face from all vertex groups after the first extrusion (i == 0)
		# This way it can't get extruded again
		# Because the edge of the first face can be part of multiple groups
#		if not i:
#			bpy.ops.object.vertex_group_remove_from()
	
		bpy.ops.object.mode_set(mode='OBJECT')
		bpy.ops.object.mode_set(mode='EDIT')
		
#		newValue = (math.sin(math.radians(t)+math.sin(math.radians(t/2.3)))+2.0)*mainLevel
#		newRatio = newValue/oldValue
		
		newRatio = mainLevel
		# Get the mesh
		me = ob.data
		# Loop through the faces to find the center
		area = 1
		
		for p in me.polygons:
			if p.select == True:
				area = math.sqrt(p.area) * 3
			
		dZ = stepDist * area
		sX = newRatio
		sY = newRatio
		rA = modLevel*math.sin(math.radians(t*modRate))*(-stepRot/10)*dirFace
		aX = axis[0]
		aY = axis[1]
		aZ = axis[2]
				
		snglData = [dZ, sX, sY, rA, aX, aY, aZ]
		extrudeOneStep(snglData)
		
		bpy.context.object.active_material_index = lengthList[tempNum]
		bpy.ops.object.material_slot_assign()
		
#		p.material_index = lengthList[tempNum]
#		print(lengthList)
#		print(p.material_index)
		tempNum += 1
							
# firstExtrude(END) -------------------------------<

#SelectMode->FACE
def selMode2FA():
	ob = bpy.context.object
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_mode(type='FACE')
	bpy.ops.object.mode_set(mode='OBJECT')
	
#SelectMode->VERT
def selMode2VT():
	ob = bpy.context.object
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_mode(type='VERT')
	bpy.ops.object.mode_set(mode='OBJECT')
	
#SelectMode->Edit
def selMode2Edit():
	object = bpy.context.active_object
	bpy.ops.object.mode_set(mode='EDIT')

#SelectMode->Object
def selMode2Object():
	object = bpy.context.active_object
	bpy.ops.object.mode_set(mode='OBJECT')

# select faces from Groups --------------------------------------------->
def selPart(vGroup):
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.object.vertex_group_set_active(group=vGroup)
	bpy.ops.object.vertex_group_select()
	
# select faces from By materialNum --------------------------------------------->
def selPartByNum(mNum):
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.context.object.active_material_index = mNum
	bpy.ops.object.material_slot_select()
	bpy.ops.object.mode_set(mode='OBJECT')

# deselectAllMesh------------------------------------------>
def deselectAllMesh():
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode='OBJECT')
	
# AddSubsurfModifier --------------------------------------------->
def addSubsurf(level):
	bpy.ops.object.modifier_add(type='SUBSURF')
	object = bpy.context.active_object
	object.modifiers['Subsurf'].levels = level

#Make materials
def makeMaterials(num):
	for i in range(num + 1):
		matNum = i	
		matName = 'material' + str(matNum)

		mat = bpy.data.materials.new(matName)
		if i == 0:
			mat.diffuse_color = (1.0, 1.0, 1.0)
		else:
			mat.diffuse_color = (random.random(), random.random(), random.random())
			
		mat.texture_slots.add()

		obj = bpy.context.scene.objects.active
		obj.data.materials.append(mat)
	
	matName = 'temp'

	mat = bpy.data.materials.new(matName)
	mat.diffuse_color = (random.random(), random.random(), random.random())
	mat.texture_slots.add()

	obj = bpy.context.scene.objects.active
	obj.data.materials.append(mat)

# Make morphData ###
def makeMorphData():
	d0 = 0
	d1 = random.uniform(0.1, 0.4)
	d2 = random.randint(3, 8)
	d3 = random.uniform(-0.5, -0.2)
	d4 = random.randint(-90, 90)
	d5 = random.randint(-180, 180)
	d6 = random.randint(-60, 60)
	d7 = random.uniform(0.1, 0.8)
	d8 = random.uniform(0.1, 0.8)
	d9 = [random.uniform(-1, 1), random.uniform(-1, 1), random.uniform(-1, 1)]
#	d9 = [-1, 0, 0]
	d10 = 0
	d11 = 0
	d12 = 0
	
	morphData = [d0, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12]
	return morphData

### Generate Object!!! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<###
def generate(legNum, legType, underType01, underType02, upperType, sideType, topType):
#	random.seed(seedNum)
	for item in bpy.data.materials:
		ob = bpy.context.object
		bpy.data.materials.remove(item)

	obX = 0
	obY = 0
	bodyNum = 0
	
	n = legNum # <<<<<<<<<<<<<<<       Number of legs     <<<<<<<<<<<<<
	
#	n = random.randint(2, 5) * 2 + 1
	
	bpy.ops.mesh.primitive_circle_add(vertices=n, fill_type='TRIFAN', view_align=False, enter_editmode=False, location=(obX, obY, 0), layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
	addSubsurf(1)
	bpy.context.object.rotation_euler[2] = -1.5708
	matNum = 10
	makeMaterials(matNum)

	growStage(n, legType)
	
	bpy.ops.object.mode_set(mode='OBJECT')
	bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Subsurf")
##	addSubsurf(2)
	bpy.ops.object.mode_set(mode='EDIT')
	
#	morphData = [0, 0.8, 8, -1.5, 45.0, 160.0, 60.0, 0.05, 0.05, [-1, 0, 0], 0, 0, 0]
#	morphData = [0, 0.8, 8, -1.5, 10.0, 90.0, 10.0, 0.3, 0.3, [-1, 0, 0], 0, 0, 0]
	
	selMode2FA()
	deselectAllMesh()
	selMode2Edit()
	bpy.ops.mesh.select_all(action='SELECT')
	bpy.ops.mesh.normals_make_consistent(inside=False)
	bpy.ops.mesh.select_all(action='DESELECT')

# Leg under	
	bpy.context.object.active_material_index = 1
	bpy.ops.object.material_slot_select()

	selMode2Object()
	_aln = 0.07 #0.07
	mData = [[7, 0.2, 2, -0.3, 0.0, -90.0, 0.0, 0.2, 0.2, [-1, 0, 0], 0, 0, _aln],     [7, 0.3, 2, -0.1, 30.0, 0.0, 250.0, 0.2, 0.2, [-1, 0, 0], 0, 0, _aln],                       [7, 0.4, 1, -0.2, 50.0, 90.0, 40.0, 0.2, 0.2, [-1, 0, 0], 0, 0, _aln],                      [7, 0.5, 1, 0.2, 80.0, -50.0, 80.0, 0.2, 0.2, [-1, 0, 0], 0, 0, _aln],                         [7, 0.5, 2, -0.05, 30.0, 120.0, -220.0, 0.2, 0.2, [-1, 0, 0], 0, 0, _aln]  ]
	morphData = mData[underType01]
	multiExtrudeByMaterialNum(morphData)	
	bpy.ops.mesh.select_all(action='DESELECT')	
	
# Leg under	2
	bpy.context.object.active_material_index = 7
	bpy.ops.object.material_slot_select()

	selMode2Object()
	_aln = 0.07 #0.07
	mData = [[1, 0.3, 4, -0.2, 0.0, 240.0, 80.0, 0.2, 0.2, [0, 0, 0], 0, 0, _aln],     [1, 0.2, 4, -0.2, 30.0, -220.0, 240.0, 0.2, 0.3, [1, 0, 0], 2, 0, _aln],                        [1, 0.3, 3, -0.2, 10.0, 220.0, 40.0, 0.4, 0.3, [0, 0, 1], 0, 0, _aln],                      [1, 0.15, 4, -0.5, -40.0, -160.0, -80.0, 0.25, 0.25, [0, -1, 0], 0, 0, _aln],                     [1, 0.2, 2, 0.1, 0.0, 0.0, 0.0, 0.1, 0.1, [0, 0, 0], 0, 0, _aln]  ]
	morphData = mData[underType02]
	multiExtrudeByMaterialNum(morphData)	
	bpy.ops.mesh.select_all(action='DESELECT')	
	
# Leg side	
	bpy.context.object.active_material_index = 3
	bpy.ops.object.material_slot_select()

	selMode2Object()
	_aln = 0.1 #0.1
	mData = [[0, 0.2, 5, -0.2, 20.0, 60.0, 70.0, 0.2, 0.3, [-1, 0, 0], 0, 0, _aln],    [0, 0.2, 5, -0.2, 45.0, -120.0, -90.0, 0.2, 0.2, [-1, 0, 0], 0, 0, _aln],                    [0, 0.2, 6, -0.4, 20.0, -150.0, -60.0, 0.2, 0.2, [-1, 0, 0], 0, 0, _aln],                  [0, 0.15, 3, -0.4, 30.0, 150.0, -120.0, 0.3, 0.2, [-1, 1, 0], 0, 0, _aln],                [0, 0.2, 6, -0.25, 50.0, 170.0, -50.0, 0.2, 0.1, [-1, 0, 0], 0, 0, _aln]   ]
	morphData = mData[sideType]
	multiExtrudeByMaterialNum(morphData)	
	bpy.ops.mesh.select_all(action='DESELECT')
	
# Leg upper	
	bpy.context.object.active_material_index = 2
	bpy.ops.object.material_slot_select()

	selMode2Object()
	_aln = 0.1 #0.1
	mData = [[6, 0.3, 6, -0.2, -40.0, -60.0, -45.0, 0.2, 0.2, [-1, 0, 0], 0, 0, _aln],  [6, 0.3, 9, -0.2, -40.0, 140.0, -40.0, 0.3, 0.2, [-1, 0, 0], 2, 0, _aln],                       [6, 0.2, 5, -0.4, 30.0, -120.0, 20.0, 0.2, 0.3, [-1, -1, 0], 2, 0, _aln],                     [6, 0.2, 9, -0.3, 15.0, 180.0, 120.0, 0.3, 0.2, [-1, 0, 0], 0, 0, _aln],                     [6, 0.3, 9, -0.3, 20.0, 180.0, 60.0, 0.2, 0.2, [1, 0, 0], 0, 0, _aln]   ]
	morphData = mData[upperType]
	multiExtrudeByMaterialNum(morphData)	
	bpy.ops.mesh.select_all(action='DESELECT')	
	
# Top	
	bpy.context.object.active_material_index = 4
	bpy.ops.object.material_slot_select()

	selMode2Object()
	_aln = 0.1 #0.1
	mData = [[0, 0.4, 9, -0.3, -20.0, 180.0, 60.0, 0.2, 0.2, [1, 0, 0], 0, 0, _aln],     [0, 0.3, 5, -0.4, -30.0, 60.0, 50.0, 0.2, 0.3, [0, 1, 0], 0, 0, _aln],                          [0, 0.4, 7, -0.4, -40.0, 240.0, -60.0, 0.1, -1.0, [0, 1, 0], 0, 0, _aln],                           [0, 0.3, 7, -0.4, 50.0, 100.0, -50.0, 0.1, -1.3, [0, 1, 0], 0, 0, _aln],                          [0, 0.3, 5, -0.4, 60.0, 90.0, 60.0, -0.15, 1.2, [0, 1, 0], 2, 0, _aln]   ]
	morphData = mData[topType]
	multiExtrudeByMaterialNum(morphData)	
	bpy.ops.mesh.select_all(action='DESELECT')	
		
	selMode2Object()
	addSubsurf(2)
	paramCombi = str(legNum)+str(legType)+str(underType01)+str(underType02)+str(upperType)+str(sideType)+str(topType)
	bpy.context.object.name = paramCombi

	for i in range(matNum + 2):
		bpy.ops.object.material_slot_remove()
		
#----------------------------------------------------------------- Main Code to here

if __name__ == '__main__':
#	'''
	
	bpy.types.Scene.legNum = bpy.props.IntProperty(name="legNUM", default = 5, min = 3, max = 10, description = "legNum")
	bpy.types.Scene.LEG = bpy.props.IntProperty(name="LEG", default = 0, min = 0, max = 9, description = "LEG")
	bpy.types.Scene.underType_01 = bpy.props.IntProperty(name="underType_01", default = 0, min = 0, max = 4, description = "underType01")
	bpy.types.Scene.underType_02 = bpy.props.IntProperty(name="underType_02", default = 0, min = 0, max = 4, description = "underType02")
	bpy.types.Scene.upperType = bpy.props.IntProperty(name="upperType", default = 0, min = 0, max = 4, description = "upperType")
	bpy.types.Scene.sideType = bpy.props.IntProperty(name="sideType", default = 0, min = 0, max = 4, description = "sideType")
	bpy.types.Scene.topType = bpy.props.IntProperty(name="topType", default = 0, min = 0, max = 4, description = "topType")

#	'''
	register()
